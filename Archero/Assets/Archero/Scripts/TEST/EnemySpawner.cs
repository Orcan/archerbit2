using System.Collections;
using System.Collections.Generic;
using Orcan.TEST;
using UnityEngine;
using Zenject;

public class EnemySpawner : ITickable
{
    readonly Enemy.Factory _enemyFactory;
    public readonly int MIN_ENEMY_SPEED = 10;
    public readonly int MAX_ENEMY_SPEED = 50;
    public int ShouldSpawnCount = 0;
    public bool ShouldSpawnNewEnemyBool = true;
    public EnemySpawner(Enemy.Factory enemyFactory)
    {
        _enemyFactory = enemyFactory;
    }
    bool ShouldSpawnNewEnemy()
    {
        if (ShouldSpawnCount == 5 || ShouldSpawnNewEnemyBool == false)
        {
            ShouldSpawnNewEnemyBool = false;
            return false;
        }
        Debug.Log("   TUT SRABOTALO!!!!! ");
        return true;
    }
    public void Tick()
    {
        if (ShouldSpawnNewEnemy())
        {
            ShouldSpawnCount += 1;
            var newSpeed = Random.Range(MIN_ENEMY_SPEED, MAX_ENEMY_SPEED);
            var enemy = _enemyFactory.Create(newSpeed);
        }
    }
}