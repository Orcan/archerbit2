using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orcan.Attack;
using Orcan.AI;
using UniRx;
using Zenject;
using Orcan.Infrastructure;

namespace Orcan.Unit
{
    public class TargetAI : AIContoller, IEntityComponent
    {
        Vector2 xAreaGames;
        Vector2 zAreaGames;
        public GameObject AreaGames;
        public GameObject[] TargetsArea;
        public int TargetsAreaCount = 0; 
        Entity entity; 
        public Vector2 Move2;

        LevelManager levelManager;
        AreaGames areaGames;
         
        [Inject]
        public void Construct(LevelManager _levelManager, AreaGames _areaGames)
        {
            levelManager = _levelManager;
            areaGames = _areaGames;
        }
         
        public virtual void MoveTargets()  
        {    
            if (Vector3.Distance(entity.gameObject.transform.position, TargetsArea[TargetsAreaCount].gameObject.transform.position) > 1)
            {    
                Move();
            }
            else
            {// если находится то + 1 Таргет 
                TargetsAreaCount += 1;
                if (TargetsArea.Length - 1 < TargetsAreaCount)
                {
                    TargetsAreaCount = 0;
                }
                Move2.x = TargetsArea[TargetsAreaCount].gameObject.transform.position.x;
                Move2.y = TargetsArea[TargetsAreaCount].gameObject.transform.position.z;
            }
        } 
        public override void Init(Entity _entity)
        {
            Move2.x = TargetsArea[TargetsAreaCount].gameObject.transform.position.x;
            Move2.y = TargetsArea[TargetsAreaCount].gameObject.transform.position.z;

            entity = _entity;
            UpdateFireON();

            xAreaGames = areaGames.xAreaGames;
            zAreaGames = areaGames.zAreaGames;
            AreaGames = areaGames.areaGames; 
        }

        public virtual void Move() // только если включено все
        {
            if (entity.movement != null && entity.movement.agent != null)
            {
                entity.movement.agent.SetDestination(new Vector3(Move2.x,
                     areaGames.areaGames.transform.position.y, Move2.y));
            }
        }

        void UpdateFireON()
        {
            if (entity.attack != null)
            {
                var clickStream = Observable.EveryUpdate()
                            .RepeatUntilDestroy(this)
                            .Subscribe(x =>
                            FireIF()
                            );
            }
            else
            {
                Invoke(nameof(UpdateFireON), 2);
            }
        }

        private void FireIF()
        {
            if (entity.gameObject.activeSelf == true)
            {
                MoveTargets();
                if (entity.attack.cooldawnIsActive == true
                    && entity.attack.targetAttackIS == true)
                {
                    entity.attack.Fire();
                }
            }
        }
 
    }

}


