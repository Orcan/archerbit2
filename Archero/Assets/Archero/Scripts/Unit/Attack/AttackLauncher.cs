using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orcan.Unit;
using Zenject;

namespace Orcan.Attack
{
    public enum BulletTypes
    {
        FromEnemy,
        FromPlayer
    }
    public class AttackLauncher : MonoBehaviour, IEntityComponent // bullet 
    {   // какой Игрок сделал 
        Attack attack;
        Entity entity;
        [SerializeField] public GameObject bulletPrefab;
        // [SerializeField] private Bullet bullet => GetComponent<Bullet>();
        [SerializeField] public Transform attackLauncherGameObject;
        public float _powerAtak = 1;  // сила атаки при ударе об другой объект
                                      //   public int FactionID = 0;  
        public float _speedBullet = 10;
        public float _lifeTimeBullet = 3;
         
        Bullet.Factory _bulletFactory;
        [Inject]
        public void Construct(Bullet.Factory bulletFactory)
        {
            _bulletFactory = bulletFactory;
        }


        public void Init(Entity _entity)
        {
            entity = _entity;
            attack = entity.attack;
            attack.FireEvent.AddListener(AtackEnemy);  // подписаться к Эвенту attack.Fire
        }

        public void AtackEnemy()
        {
 
            var bullet = _bulletFactory.Create(
            _powerAtak, _speedBullet, _lifeTimeBullet , entity.FactionID, attackLauncherGameObject.transform.eulerAngles.y);
    
            bullet.transform.position = gameObject.transform.position;
        
        }

        public void SetTarget(Entity entity)
        {
        }

        public void OnEntityPreInit()
        {
        }  
    }

}