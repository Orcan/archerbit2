 
using UnityEngine;
 
using Zenject;

public class TestSignalPress : MonoBehaviour, ITickable, IInitializable
{
//    [Inject]
//    private CountPressButSignal countPressButSignal;
    public int press = 0;

    void Start()
    {
        // countPressButSignal.Listen(); //  Подписаться на J  
        // countPressButSignal.UnListen(); // Отписаться от нажатий B
        // countPressButSignal.Fire();    // Возможно на нажатие кнопки подписаться на каждые 10 20 30 40 50 нажатий на А 
    }
    public void Initialize()
    {
        //   throw new System.NotImplementedException();
    }

    void ITickable.Tick()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            CountPress();
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            CountPressButSignalLisen();
            Debug.Log(" CountPressButSignalLisen();  ");
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            CountPressButSignalUnLisen();
            Debug.Log(" CountPressButSignalUnLisen();  ");
        }
    }
    private void CountPressButSignalLisen()
    {
 //       countPressButSignal.Listen(); //  Подписаться на J  
    }
    private void CountPressButSignalUnLisen()
    {
  //      countPressButSignal.UnListen(); // Отписаться от нажатий B
    }

    private void CountPress()
    {
        press += 1;
        CountPressButSignalFire();
    }
    public void CountPressButSignalFire()
    {
        if (press % 10 == 0)
        {
   //         countPressButSignal.Fire();    // Возможно на нажатие кнопки подписаться на каждые 10 20 30 40 50 нажатий на А 
   //         countPressButSignal.TryFire<CountPressButSignal>();    // Возможно на нажатие кнопки подписаться на каждые 10 20 30 40 50 нажатий на А 
                                                                   //   Debug.Log(" press Кратное 10 =  " + press);
        }
    }
}
