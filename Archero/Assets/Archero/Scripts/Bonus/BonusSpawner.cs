 
using UnityEngine;
using Zenject; 

public class BonusSpawner : MonoBehaviour
{
    AreaGames areaGames;
    GameObject bonusPrefab;

    Bonus.Factory bonusFactory; 
    [Inject] 
    public void  Construct(AreaGames _areaGames, Bonus.Factory _bonusFactory)
    {
        areaGames = _areaGames;
        bonusFactory = _bonusFactory;
    }
    private void Start()
    {
        RepeatSpawn();
    }
     
    void StopRepeatSpawn()
    {
        CancelInvoke(nameof(SpawnBonus));
    }
    void RepeatSpawn(float _delay= 0 , float _repeat = 5)
    {
        InvokeRepeating(nameof(SpawnBonus), _delay, _repeat); 
    }


     void SpawnBonus() /// тут нужно определиться когда как часто будут  Спаувниться 
    { 
        float x = Random.Range(areaGames.xAreaGames.x, areaGames.xAreaGames.y);  
        float z = Random.Range(areaGames.zAreaGames.x, areaGames.zAreaGames.y); 
        var bonus = bonusFactory.Create(x, areaGames.areaGames.transform.position.y + 1, z); // сюда прийдет новое 
         
    }

 
}
