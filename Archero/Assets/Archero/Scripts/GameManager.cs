using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Orcan.Attack;
using UniRx;
using UnityEngine;
namespace Orcan.Unit
{
    public class GameManager1 : MonoBehaviour
    {
        public List<Entity> Units = new List<Entity>();


        public GameObject ManPrefab;
        public GameObject bonusPrefab;
        public BonusSpawner bonusSpawner;
         
        public AreaGames areaGames;

        public int CountPlayerInTable = 1;
        public int countForWin = 5;
        public int spawnPoint = 1;
        public float respawnTimeUnitMan = 7;


        public List<Entity> players = new List<Entity>();
        public List<Bullet> bullet = new List<Bullet>();

        public List<int> Player;

        private bool EndGame = false;
        public static GameManager1 singl;
        public void Start()
        {
            PlayerRegistr();
            areaGames = gameObject.AddComponent<AreaGames>();
            areaGames.CoordinatesAreaGames();
            singl = this;
            InvokeRepeating(nameof(FindEnemy), 0, 3);
            UnitsInits();
       //     bonusSpawner.Init(areaGames, bonusPrefab, 5f);
            PointIn(-1);

        }
       
        public void PointIn(int FactionID)
        {
            if (EndGame) { return; }
            if (FactionID != -1) Player[FactionID] += 1;

            string textString = "Score: \n";
            for (int i = 0; i < CountPlayerInTable; i++)
            {
                if (Player[i] >= countForWin)
                {
                    textString = "ПОБЕДИЛ ИГРОК №" + i;
              //      leaderboardUI.TextIN(textString);
                    EndGame = true;
                    DestroyAllUnit();

                    return;
                }

                textString += "Player" + i + ":  " + Player[i] + "\n";

            }
       //     leaderboardUI.TextIN(textString);
        }

        void DestroyAllUnit()
        {
            for (int i = 0; i < Units.Count; i++)
            {
                Destroy(Units[i]);
            }
        }



        private void UnitsInits()
        {
            for (int i = 0; i < Units.Count; i++)
            {
                Units[i].Init();
            }
        }
        private void PlayerRegistr()
        {
            for (int i = 0; i < CountPlayerInTable; i++)
            {
                Player.Add(0);
            }
        }

        void FindEnemy() //   AIContoller
        {
            Entity[] arr = FindObjectsOfType<Orcan.Unit.Entity>();
            players = arr.Cast<Entity>().ToList();
        }

        /// /////////////// ///////////// //////////// //////////// ///////////////// ///////////// ///////// 

        //public void DeadThisGo()
        //{
        //    GameObject Finish = GameObject.FindWithTag("Finish");
        //    // transform.position = new Vector3(1000, 1000, 1000);
        //    this.gameObject.transform.SetParent(Finish.transform);
        //    this.gameObject.SetActive(false);
        //    Observable.Timer(System.TimeSpan.FromSeconds(respawnTimeUnitMan)).Subscribe(_ => { this.gameObject.SetActive(true); Respawn(); }).AddTo(this.gameObject);
        //}

        //public void Respawn()
        //{
        //    //GameObject cloneBullet = Instantiate(ManPrefab, attackLauncherGameObject.position, attackLauncherGameObject.rotation) as GameObject;
        //    //Bullet bullet = cloneBullet.GetComponent<Bullet>();

        //    //currentHealth = maxHealth;
        //    //GameObject RespawnGO = GameObject.FindWithTag("Respawn");
        //    //x = Random.Range(xAreaGames.x, xAreaGames.y);
        //    //z = Random.Range(zAreaGames.x, zAreaGames.y);
        //    //this.gameObject.transform.position = new Vector3(x, AreaGames.transform.position.y, z);
        //    //this.gameObject.transform.SetParent(RespawnGO.transform);
        //    //AIStart();

        //    //if (whoControls == WhoControls.AI) { aiEnemy.EndRespawn(); }
        //}


    }
}