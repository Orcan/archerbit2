using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Orcan.Unit
{
    public class UnitMovement : MonoBehaviour
    {
        public Unit unit { private set; get; }
        public Movement moveContooller { private set; get; }

        public GameObject Target;

        public bool DestantionReached { private set; get; }
        public Vector3 Destantion => Target.transform.position;

        public void Stop() { }
        public void MoveToTarget() { }

        public void UpdateRotation() { }

        public void OnPathDistantion() { }

         
    }
}