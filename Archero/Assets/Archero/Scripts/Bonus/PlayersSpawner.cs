
using System.Collections.Generic;
using Orcan.Unit;
using UnityEngine;
using Zenject;

public class PlayersSpawner : MonoBehaviour
{
    [SerializeField]
    public List<Player> players; 

    AreaGames areaGames; 
    Entity.Factory entityFactory;
    
   public  void SpawnPlayers() /// тут нужно определиться когда как часто будут  Спаувниться 
    { 
        float x = Random.Range(areaGames.xAreaGames.x, areaGames.xAreaGames.y);
        float z = Random.Range(areaGames.zAreaGames.x, areaGames.zAreaGames.y);
        var Unit = entityFactory.Create(new Vector3(x, areaGames.areaGames.transform.position.y + 1, z), players[0].FactionID, players[0].playerPrefab); // сюда прийдет новое  
    }

   
}

[System.Serializable]
public class Player
{ 
    public int FactionID =0;
    public GameObject playerPrefab;  
}

