using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Zenject;
namespace Orcan.Infrastructure
{
    public class LeaderboardUI : MonoBehaviour
    {
        public TextMeshProUGUI TextLeaderboardUI;
        LevelManager sceneManager;
        private bool EndGame = false;
        List<int> Player = new List<int>();
         
        SignalBus signalBus;
        [Inject]
        public void Construct(LevelManager _sceneManager, SignalBus _signalBus)
        {
            sceneManager = _sceneManager;
            signalBus = _signalBus;
        }
        private void Start()
        {
            signalBus.Subscribe<PlayerIdKillSignal>(PointIn);
        }

        public void TextIN(string _text)
        {
            TextLeaderboardUI.text = _text;
        }
        public void PointIn(PlayerIdKillSignal arg)
        {
            if (EndGame) { return; }
            if (arg.FactionID != -1) Player[arg.FactionID] += 1;

            string textString = "Score: \n";
            for (int i = 0; i < sceneManager.CountPlayerInTable; i++)
            {
                if (Player[i] >= sceneManager.countForWin)
                {
                    textString = "ПОБЕДИЛ ИГРОК №" + i + 1;
                    TextIN(textString);
                    EndGame = true;
                    DestroyAllUnit();

                    return;
                }

                textString += "Player" + i + ":  " + Player[i] + "\n";

            }
            TextIN(textString);
        }

        void DestroyAllUnit()
        {
            for (int i = 0; i < sceneManager.Units.Count; i++)
            {
                Destroy(sceneManager.Units[i]);
            }
        }

        public void PlayerRegistr()
        {
            for (int i = 0; i < sceneManager.CountPlayerInTable; i++)
            {
                Player.Add(0);
            }
        }
    }
}