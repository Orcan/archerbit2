using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orcan.Unit;
using Zenject;

namespace Orcan.Attack
{
    public class Bullet : MonoBehaviour, IPoolable<float, float, float, int, float, IMemoryPool>
    {
        public float powerAtak = 1;  // сила атаки при ударе об другой объект
        public float speed = 1;
        public float destroyTime = 3;
        public int FactionID = 0;
        Rigidbody RB; 
        IMemoryPool _pool;

        public void OnSpawned(float _powerAtak, float _speed, float _destroyTime, int _FactionID, float _Rotation, IMemoryPool pool)
        { 
            transform.rotation = Quaternion.Euler(transform.rotation.x, _Rotation, transform.rotation.z);
            if (RB == null) RB = GetComponent<Rigidbody>();

            RB.velocity = transform.forward * _speed; 

            powerAtak = _powerAtak;
            _pool = pool;
            speed = _speed;
            destroyTime = _destroyTime;
            FactionID = _FactionID;
           //destroyTime _startTime = Time.realtimeSinceStartup;
        }

        public void OnDespawned()
        {
            _pool = null;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "block")
            {
                RB.velocity = transform.forward * 0; 
                powerAtak = 0;
            }
            else if (other.GetComponent<Entity>() != null && other.GetComponent<Entity>().FactionID != FactionID)
            {
                other.GetComponent<Entity>().Health.Add(-powerAtak, FactionID);
                _pool.Despawn(this);// тут должен быть в пулл а Дестрой
            }
        }
        public class Factory : PlaceholderFactory<float, float, float, int, float, Bullet>
        {
        }


        //public void Init(float _powerAtak, float _speed, float _destroyTime, int _FactionID)
        //{
        //    powerAtak = _powerAtak;
        //    speed = _speed;
        //    destroyTime = _destroyTime;
        //    FactionID = _FactionID;
        //    RB = GetComponent<Rigidbody>();
        //    RB.velocity = transform.forward * speed;
        //    Destroy(this.gameObject, destroyTime); // тут должен быть в пулл а Дестрой
        //} 
        //public void Update()
        //{

        // //   transform.position -= transform.right * speed * Time.deltaTime;

        //    if (Time.realtimeSinceStartup - _startTime > destroyTime)
        //    {
        //        _pool.Despawn(this);
        //    }
        //}
    }


}