using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaGames : MonoBehaviour
{
    public Vector2 xAreaGames;
    public Vector2 zAreaGames;  
    public GameObject areaGames => GameObject.FindWithTag("AreaGames");
      
  public   void CoordinatesAreaGames()
    {
        float width = areaGames.transform.localScale.x;// ширина
        float height = areaGames.transform.localScale.z; // высота

        float xc = areaGames.transform.position.x;
        float yc = areaGames.transform.position.z;//- координаты центра.

        float xl = xc - width / 2; //"левый" x Верхний 
        float xr = xc + width / 2; //"правый" x Верхний   

        float yt = yc + height / 2;//"верхний" Z  
        float yb = yc - height / 2;//"нижний"Z

        xAreaGames.x = xl;
        xAreaGames.y = xr;

        zAreaGames.x = yt;
        zAreaGames.y = yb;
    }
}
