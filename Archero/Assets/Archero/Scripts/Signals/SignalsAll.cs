using UnityEngine;
using Zenject; 
public class PlayerIdKillSignal : Signal<PlayerIdKillSignal>
{
    public int FactionID;
    public PlayerIdKillSignal(int _FactionID) { FactionID = _FactionID; }
}
