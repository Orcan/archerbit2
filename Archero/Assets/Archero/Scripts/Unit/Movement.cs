using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Orcan.Unit
{
    public class Movement : MonoBehaviour, IEntityComponent
    {
        Entity entity;
        public float speed = 5;
        public NavMeshAgentController navMeshAgentController;
        public NavMeshAgent agent;  

        public virtual void Init(Entity _entity)
        {
            entity = _entity; 
            entity.gameObject.GetComponent<NavMeshAgent>();
            entity.gameObject.GetComponent<NavMeshAgent>().speed = speed; 
        }
        public void OnEntityPreInit()
        {
        }
        public void SetTarget(Entity entity)
        {
        }

        public void SetMovement(Entity entity)
        {
            // куда перейти 
        } 
    }
}