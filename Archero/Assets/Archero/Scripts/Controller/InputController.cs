using System.Collections;
using System.Collections.Generic;
using Orcan.Unit;
using UnityEngine;
using Zenject;
using static Orcan.Infrastructure.LevelManager;

namespace Orcan.Infrastructure
{
    public class InputController : MonoBehaviour, IEntityComponent
    {
        Entity entity;
        bool Initialisabe = false;

        IControllerUnit _ControllerUnit;
        [Inject] VariableJoystick variableJoystick;
        [Inject] LevelManager levelManager;
        public void Init(Entity _entity)
        { 
            entity = _entity;
            if (Initialisabe == false)
            {
                Initialisabe = true;
                if (levelManager.platformControl == Platform.Android) { var h = gameObject.AddComponent<PlayerMobileController>(); h.Init(_entity, variableJoystick); }
                else if (levelManager.platformControl == Platform.Windows) { var h = gameObject.AddComponent<PlayerWinwowsController>(); h.Init(_entity); }
                else if (levelManager.platformControl == Platform.IMac) { var h = gameObject.AddComponent<PlayerIMacController>(); h.Init(_entity); }// добавить управление Text на 20 секунд
                _ControllerUnit = GetComponent<IControllerUnit>();
            }
            else
            {
                _ControllerUnit.PostInit();
            }
        }

        public void OnEntityPreInit()
        {
        }
    }
}