using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orcan.Unit;
using UnityEngine.AI;
using Orcan.Infrastructure;
using Zenject;

namespace Orcan.Attack
{
    public class UnitAttack : Attack
    {
        public float followDitance = 15; // дистация приследования
        public AttackLauncher attackLauncherTra;
        public List<Entity> targets = null;
        public List<Entity> targetsFilter = null;
       
        public LevelManager levelManager; // LevelManager
        [Inject]
        public void Construct(LevelManager _levelManager)
        {
            levelManager = _levelManager; 
        }


        public override void Init(Entity _entity)
        {
            base.Init(_entity);
            FindEnemy();  
        }

        private void LateUpdate()
        {
            FindTargetAndView();
            if(targetAttack != null && targetAttack.gameObject.activeSelf == true)
            {
                targetAttackIS = true;
            }
            else
            {
                targetAttackIS = false;
            }

        }

        void FindTargetAndView() 
        {
            Transform TargetEnemy = null;
            GetCurrentTarget(); // тут делегат должен быть с подпиской кто будет Таргет
                                //  StartCoroutine(GetCurrentTarget());

            if (targetAttack == null)
            {
                return;
            }

            if (targetAttack != null && targetAttack.gameObject.activeSelf) // поворот пушки 
            {
                Transform shotSpawn = attackLauncherTra.attackLauncherGameObject;
                TargetEnemy = targetAttack.transform;
                shotSpawn.transform.rotation = Quaternion.RotateTowards(shotSpawn.transform.rotation, Quaternion.LookRotation(TargetEnemy.transform.position - shotSpawn.transform.position), 10 * Time.deltaTime * 100);
                shotSpawn.transform.rotation = Quaternion.Euler(transform.eulerAngles.x, shotSpawn.transform.eulerAngles.y, transform.eulerAngles.z);
            }
        }

        public void FindEnemy()          //     AIContoller
        {

            targets = levelManager.players;
            targetsFilter.Clear();
            //    Entity[] entity = FindObjectsOfType<Entity>();
            for (int j = 0; j < targets.Count; j++)
            { 
                if (targets[j].FactionID != entity.FactionID)
                {
                    targetsFilter.Add(targets[j]); 
                }
            }
            if (targetsFilter.Count <= 0)
            {
                StopAllCoroutines();
                StartCoroutine(GetFindEnemyAlways()); return;

            }
            //   StartCoroutine(GetCurrentMoveTarget());
            StopAllCoroutines();
            StartCoroutine(GetFindEnemyAlways());
        }

        public IEnumerator GetFindEnemyAlways()
        {
            yield return new WaitForSeconds(3f);
            FindEnemy(); 
            // kputo //   if (entity.GetComponent<Entity>().FactionID =   whoControls == WhoControls.AI) { aiEnemy.RandomMoveForEnemy(); }
        }
        public void GetCurrentTarget() // из списка Фильтра! чисто до кого ближе
        { 
            if (targetsFilter.Count > 0)
            {
                int minDist = 0;
                float dist = float.MaxValue;
                for (int i = 0; i < targetsFilter.Count; i++)
                {
                    if (targetsFilter[i].Health.isDead == true && targetsFilter[i] != null) { continue; }
                          float dist2 = Vector3.Distance(targetsFilter[i].transform.position, transform.position);
                    if (dist > dist2)
                    {
                        dist = dist2; minDist = i;
                    }
                }
                targetAttack = null;
                targetAttack = targetsFilter[minDist];
            } 
        } 
    


    }
}