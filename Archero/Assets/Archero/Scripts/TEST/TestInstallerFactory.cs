using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Orcan.TEST
{
    public class TestInstallerFactory : MonoInstaller
    {
        public GameObject EnemyPrefab;

        public override void InstallBindings()
        {
            Container.BindInterfacesTo<EnemySpawner>().AsSingle();
            Container.Bind<Player>().AsSingle();
            Container.BindFactory<float, Enemy, Enemy.Factory>().FromComponentInNewPrefab(EnemyPrefab); 
        }
    }
}