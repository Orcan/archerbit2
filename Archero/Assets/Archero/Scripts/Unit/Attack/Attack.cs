using System.Collections;
using System.Collections.Generic;
using Orcan.Unit;
using UnityEngine;
using UniRx;
using UnityEngine.Events;

namespace Orcan.Attack
{
    public class Attack : MonoBehaviour, IEntityComponent
    {
        public Entity entity = null;
        public bool targetAttackIS = false;
        public Entity targetAttack = null;
        public bool isTargetRange = false;

      //  public int Damage { private set; get; } = 3;
        public int Damage  = 3;
        public float reloadDurationAttack = 3f; //
                                                //   public float timerDurationAttack;
        public bool cooldawnIsActive = true;  // 
        public UnityEvent FireEvent;

        public virtual void Init(Entity _entity)
        {
            entity = _entity;
        }
        public virtual void OnEntityPreInit()
        {

        }
        public virtual void ReloadAttack() // через UnitRX или Корунтин 
        {
            //  если cooldawnIsActive = false 
            // считать reloadDurationAttack секунд  и если прошло это время ставим на  cooldawnIsActive = true
            // если нету  
        }

        public virtual void Fire()
        {
            //            Debug.Log(" Fire()");
            if (cooldawnIsActive == true)
            {
                FireEvent.Invoke();
                OnAttacked();
            }// тут должет быть метод который дает Ивенту к которому все подписываются сигнал Что нужно действовать 
        }
        public virtual void OnAttacked()
        {
            // timerDurationAttack = 0;
            cooldawnIsActive = false;  //     // Атаковать объект

            Observable.Timer(System.TimeSpan.FromSeconds(reloadDurationAttack)) // создаем timer Observable 
            .Subscribe(_ =>
            { // подписываемся
              //     Debug.Log(" Fire(333333)");
                cooldawnIsActive = true;
            }).AddTo(this.gameObject); // привязываем подписку к disposable
        }
 

        public void SetTarget(Unit.Entity entity)
        {
            // получить обхект чтоб атаковать 
        }

        protected virtual void OnEnterTargetRange() //потом когда будет колайдер атаки
        {
            // дотронулся до колайдера
        }
        protected virtual void OnAttackedStop()
        {
            // перестать стрелять так как внедипазона 
        }




    }
}