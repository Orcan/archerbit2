using System;
using Orcan.Attack;
using Orcan.Unit;
using UnityEngine;
using Zenject;

public class SceneInstaller : MonoInstaller
{

    public GameObject PlayerFacadePrefab;
    public GameObject EnemyFacadePrefab;
    public GameObject BulletPrefab;
    public GameObject BonusPrefab;
    public GameObject EntityPrefab;

    public override void InstallBindings()
    {
        Container.Bind<AreaGames>().FromNewComponentOn(this.gameObject).AsSingle().NonLazy();
        SpawnBonus();
        BulletPoolInstal();
        BindSignal();

    }

    void SpawnBonus()
    {
        Container.Bind<BonusSpawner>().FromNewComponentOn(this.gameObject).AsSingle().NonLazy();
        Container.BindFactory<float, float, float, Bonus, Bonus.Factory>().FromComponentInNewPrefab(BonusPrefab);
    }

    void BulletPoolInstal()
    {
        Container.BindFactory<float, float, float, int, float, Bullet, Bullet.Factory>()
            .FromPoolableMemoryPool<float, float, float, int, float, Bullet, BulletPool>(poolBinder => poolBinder
             .WithInitialSize(5)
             .FromComponentInNewPrefab(BulletPrefab)
             .UnderTransformGroup("Bullets"));
    }

    void PlayerSpawn()
    {
        Container.BindFactory<Vector3, float, GameObject, Entity, Entity.Factory>().FromComponentInNewPrefab(EntityPrefab);
    }

    class BulletPool : MonoPoolableMemoryPool<float, float, float, int, float, IMemoryPool, Bullet>
    {
    }

    void BindSignal()
    {
        SignalBusInstaller.Install(Container);
        Container.DeclareSignal<PlayerIdKillSignal>();
    }

}




