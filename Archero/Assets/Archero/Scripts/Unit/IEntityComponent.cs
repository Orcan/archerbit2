 
namespace Orcan.Unit
{
    public interface IEntityComponent
    { 
        public void OnEntityPreInit();
        public void Init(Entity _entity);
    }
}