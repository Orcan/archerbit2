using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Orcan.Attack;
using Orcan.Unit;
using UniRx;
using UnityEngine;
using Zenject;
namespace Orcan.Infrastructure
{
    public class LevelManager : MonoBehaviour
    {

        public List<Entity> Units = new List<Entity>();

        public GameObject ManPrefab;     
        public GameObject bonusPrefab;   

        public int CountPlayerInTable = 1;
        public int countForWin = 5;
        public int spawnPoint = 1;
        public float respawnTimeUnitMan = 7;
        public enum Platform { Android, Windows, IMac };
        public Platform platformControl = Platform.Android; 

        [HideInInspector] public List<Entity> players = new List<Entity>();
        [HideInInspector] public List<Bullet> bullet = new List<Bullet>();
        LeaderboardUI leaderboardUI;
        AreaGames areaGames;
        SignalBus signalBus;
        VariableJoystick variableJoystick; 
        [Inject]
        public void Construct(AreaGames _areaGames, LeaderboardUI _leaderboardUI, SignalBus _signalBus, VariableJoystick _variableJoystick  
            )
        {
            areaGames = _areaGames;
            leaderboardUI = _leaderboardUI;
            signalBus = _signalBus;
            variableJoystick = _variableJoystick; 
        }

        private void Start()
        {
            areaGames.CoordinatesAreaGames();
            leaderboardUI.PlayerRegistr();
            InvokeRepeating(nameof(FindEnemy), 0, 3);
            UnitsInits();
            if (platformControl != Platform.Android) { variableJoystick.gameObject.SetActive(false); }
            signalBus.TryFire(new PlayerIdKillSignal(-1)); 
        }

        private void UnitsInits()
        {
            for (int i = 0; i < Units.Count; i++)
            {
                Units[i].Init();
            }
        }

        /// /////////////////// ////// СТАРОЕ  Нужно переделать //////////////////////////////

        void FindEnemy()  
        {
            Entity[] arr = FindObjectsOfType<Orcan.Unit.Entity>();
            players = arr.Cast<Entity>().ToList();
        }
    }
}