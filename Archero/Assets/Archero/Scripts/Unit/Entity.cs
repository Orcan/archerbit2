using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Orcan.Attack;
using UnityEngine.Events;

namespace Orcan.Unit
{
    public class Entity : MonoBehaviour  // все характиристики 
    {
        public bool isInitialized { private set; get; }
        private string code = "entiey_name";
        public string Code => code;
        private GameObject model;
        public GameObject Model => model;
        public Rigidbody rb;
        public int FactionID = 0;
        public IEntityComponent[] entityComponents;
        public IReadOnlyDictionary<string, IEntityComponent> entityComponentsDictionary { private set; get; }
        public UnitHealth Health;
        public Movement movement = null;
        public Attack.Attack attack = null;
 
        public Entity Target = null;
         
        public void OnEntityPreInit()
        {
        }
       
        public virtual void Init()
        {
            
            rb = GetComponent<Rigidbody>();

            InitComponents(true, false);
        }

        protected void CompleteInit()
        {
            isInitialized = true;
            InitComponents(InitPre: true); 
        }

        protected void InitComponents(bool InitPre = false, bool InitPost = false)
        {
            if (InitPre)
            {
                foreach (IEntityComponent component in transform.GetComponentsInChildren<IEntityComponent>())
                {
                    component.OnEntityPreInit();
                    component.Init(this);
                    if (movement == null && component is Movement == true)
                    {
                        movement = component as Movement;

                    }
                    else if (attack == null && component is Attack.Attack == true)
                    {
                        attack = component as Attack.Attack;

                    }
                    else if (Health == null && component is UnitHealth == true)
                    {
                        Health = component as UnitHealth;
                    }
                }
            }
            PostInit();
        }

        public virtual void PostInit()
        {
        }
        protected void InitDisableComponents()
        {

            foreach (IEntityComponent component in transform.GetComponentsInChildren<IEntityComponent>())
            {
                entityComponents = null; 
            }

            if (!isInitialized)
            {
                return;
            }
            // для пост Инитилизебле
        }

        protected virtual void SubToEvent()
        { 
        }

        protected virtual void FetchComponents()
        {
            IEntityComponent[] entityComponents = transform.GetComponentsInChildren<IEntityComponent>(); 
        } 
        private void OnDestroy()
        {
           
        }
         
        public class Factory : PlaceholderFactory<Vector3, float, GameObject, Entity>// SpawnPosition , id, Prefab
        {
        }
    }
}