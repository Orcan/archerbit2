using System.Collections;
using System.Collections.Generic;
using Orcan.Unit;
using UniRx;
using UnityEngine;
using Zenject;

public class Respawn : MonoBehaviour, IEntityComponent
{
    Entity entity;
    GameObject Unit;
    public float respawnTimeUnitMan = 7;
    AreaGames areaGames;
    float x;
    float z;

    public void Init(Entity _entity) // берет 
    {
        entity = _entity; 
         Unit = entity.gameObject;
    }
    [Inject]
    public void Construct(AreaGames _areaGames)
    {
        areaGames = _areaGames; 
    }

    public void DeadThisGo()
    {
        GameObject Finish = GameObject.FindWithTag("Finish"); 
        this.gameObject.transform.SetParent(Finish.transform);
        this.gameObject.SetActive(false);
        Observable.Timer(System.TimeSpan.FromSeconds(respawnTimeUnitMan)).Subscribe(_ => { this.gameObject.SetActive(true); RespawnUnit(); }).AddTo(this.gameObject);
    }

 
        public void RespawnUnit()
    { 
        entity.Health.CurrHealth = entity.Health.MaxHealth;
        GameObject RespawnGO = GameObject.FindWithTag("Respawn");
        x = Random.Range(areaGames.xAreaGames.x, areaGames.xAreaGames.y);
        z = Random.Range(areaGames.zAreaGames.x, areaGames.zAreaGames.y);
        this.gameObject.transform.position = new Vector3(x, areaGames.transform.position.y, z);
        this.gameObject.transform.SetParent(RespawnGO.transform);  
    }


    public void OnEntityPreInit()
    {
    }



}
