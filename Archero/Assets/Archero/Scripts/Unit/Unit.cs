 
using UnityEngine; 

namespace Orcan.Unit
{
    public class Unit : Entity
    { 
    
        public MeshRenderer TextureColor;


        [Header("Status")]
        public Owner owner = Owner.Player1;
        public WhoControls whoControls = WhoControls.AI;

        public override void PostInit()
        {
       
            owner = (Owner)FactionID;
            ColorOwner c = new ColorOwner();
            TextureColor.material.color = c.GetColorPlayer(owner);
            Health.CurrHealth = Health.MaxHealth;

      
        }
       
    }
}
