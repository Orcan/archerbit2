using System.Collections;
using System.Collections.Generic;
using Orcan.Unit;
using UnityEngine;
using Zenject;
 
public class Bonus : MonoBehaviour
{
    int ran = 1;

   public float positionX;
    public float positionY;
    public float positionZ;
    [Inject]
    public void Construct(float _positionX, float _positionY, float _positionZ)
    {
        positionX = _positionX;
        positionY = _positionY;
        positionZ = _positionZ;
    }
  

    private void Start()// красный атака Зеленый скорость сделать потом черз Рендом
    {
        gameObject.transform.position = new Vector3(positionX, positionY, positionZ);
   

        ran = Random.Range(1, 3);
        if (ran == 1)
        {
            GetComponent<MeshRenderer>().material.color = Color.green;
        }
        else
            if (ran == 2)
        {
            GetComponent<MeshRenderer>().material.color = Color.red;
        } 
    }


    private void OnTriggerEnter(Collider other)
    {
        if (ran == 1)
        {
            if (other.GetComponent<Entity>() != null && other.GetComponent<Entity>().movement != null)
            {
                other.GetComponent<Entity>().movement.speed += 5;
            }
        }
        else
                 if (ran == 2)
        {
            if (other.GetComponent<Entity>() != null && other.GetComponent<Entity>().attack != null)
            {
                other.GetComponent<Entity>().attack.Damage += 5;
            }
        }

        Destroy(gameObject); // тут должен быть в пулл а Дестрой
    }

    public class Factory : PlaceholderFactory<float, float, float, Bonus> 
    {
    }

}
