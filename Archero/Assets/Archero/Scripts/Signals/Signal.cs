using UnityEngine;
using Zenject;

public interface ISignal
{
    void Fire(object TSignal);
}
 
public class Signal<T> : MonoBehaviour, ISignal where T : MonoBehaviour
{
    private SignalBus signalBus;

    [Inject]
    public void Init(SignalBus _signalBus)
    {
        signalBus = _signalBus;
    }

    public void Fire(object TSignal)
    {
        signalBus.Fire(TSignal);
    }
  


}
