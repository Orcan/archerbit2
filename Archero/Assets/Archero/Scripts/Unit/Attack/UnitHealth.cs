using Orcan.Infrastructure;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Orcan.Unit
{
    public class UnitHealth : MonoBehaviour, IEntityComponent
    {
        public float Health = 3;

        public bool isDead = false;
        public Entity entity { private set; get; }
     
        public int maxHealth = 3;
        public int MaxHealth => maxHealth; 
        public int CurrHealth = 3;
        private int InitialHealth = 3;
        public int resourceCount = 1; 
        public UnityEvent DeadEvent;   
         
         SignalBus signalBus;

        LevelManager levelManager; 
        AreaGames areaGames;

        [Inject]
        public void Construct(LevelManager _levelManager, AreaGames _areaGames, SignalBus _signalBus)
        {
            levelManager = _levelManager; 
            areaGames = _areaGames;
            signalBus = _signalBus;
        }

        public virtual void Init(Entity _entity)
        {
            entity = _entity;
            maxHealth = InitialHealth;
            isDead = false;
        }

        public void OnEntityPreInit(  Entity _entity)
        { 
            entity = _entity;
            isDead = false;
            CurrHealth = 0;
        }

   

        public void Add(float _Add, int _FactionID = 0)
        { 
            CurrHealth = CurrHealth + (int)_Add; 
            if (CurrHealth >= maxHealth)
            {
                CurrHealth = maxHealth;
            }
            if (CurrHealth <= 0)
            {
               levelManager.Units.Remove(entity); 
                signalBus.TryFire(new PlayerIdKillSignal(_FactionID));

                isDead = true;
                 
                Dead();
            }
            if (entity.isInitialized)
            {

            }
        }

        public void Destroy(Entity entity, Entity _entitySource)
        { 
            isDead = true;
        }

        public void Dead()
        {
            GameObject Finish = GameObject.FindWithTag("Finish"); 
            entity.gameObject.transform.SetParent(Finish.transform);
            entity.gameObject.SetActive(false);
            Observable.Timer(System.TimeSpan.FromSeconds( levelManager.respawnTimeUnitMan)).Subscribe(_ => { entity.gameObject.SetActive(true); Respawn(); }).AddTo(entity.gameObject);
        }

        public void Respawn()
        {
            CurrHealth = maxHealth;
            GameObject RespawnGO = GameObject.FindWithTag("Respawn");
           
            float x = Random.Range(areaGames.xAreaGames.x, areaGames.xAreaGames.y);
            float z = Random.Range(areaGames.zAreaGames.x, areaGames.zAreaGames.y);
            entity.gameObject.transform.position = new Vector3(x, areaGames.areaGames.transform.position.y, z);
            entity.gameObject.transform.SetParent(RespawnGO.transform);
            entity.Init();
        }




        public void OnEntityPreInit()
        {
        }

        public void SetMax(int value, Entity source)
        {

        }

        public bool isValid()
        {
            return true;
        }
    }
}