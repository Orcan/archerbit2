using System.Collections;
using Orcan.Unit;
using UnityEngine;

public class PlayerIMacController : MonoBehaviour, IControllerUnit
{
    Unit unit;
    Coroutine CPlayerControler;
    float Vertical = 0;
    float Horizontal = 0;

    public void Init(Entity _entity)
    {
        Entity entity = _entity;
        unit = entity as Orcan.Unit.Unit;
        // добавить управление Text на 20 секунд 
    }

    public void PostInit()
    {
        StartCoroutine(CPlayerUpDownControler());
    }


    public void StatePlayerController(bool on)
    {
        if (on) { CPlayerControler = StartCoroutine(CPlayerUpDownControler()); }
        else
        {
            if (CPlayerControler != null) StopCoroutine(CPlayerControler);
        }
    }
    IEnumerator CPlayerUpDownControler()
    {
        while (true)
        {
            yield return null;

            Vector3 direction = new Vector3(0, 0, 0);
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                Vertical = -0.9f;
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                Vertical = 0.9f;
            }

            if (Input.GetKeyUp(KeyCode.LeftArrow) && Vertical < 0)
            {
                Vertical = 0;
            }
            if (Input.GetKeyUp(KeyCode.RightArrow) && Vertical > 0)
            {
                Vertical = 0;
            }
            //////////
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                Horizontal = 0.9f;
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                Horizontal = -0.9f;
            }

            if (Input.GetKeyUp(KeyCode.UpArrow) && Horizontal > 0)
            {
                Horizontal = 0;
            }
            if (Input.GetKeyUp(KeyCode.DownArrow) && Horizontal < 0)
            {
                Horizontal = 0;
            }
            direction = Vector3.forward * Horizontal + Vector3.right * Vertical;

            unit.rb.AddForce(direction * unit.movement.speed * 2 * Time.fixedDeltaTime, ForceMode.VelocityChange);

            if (Input.GetKeyDown(KeyCode.C))
            {
                unit.attack.Fire();
            }

        }
    }
} 
