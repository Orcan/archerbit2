using System.Collections;
using System.Collections.Generic;
using Orcan.Unit;
using UnityEngine;
 
    public class PlayerWinwowsController : MonoBehaviour, IControllerUnit
    {
        Unit unit;

        float Vertical = 0;
        float Horizontal = 0;
        Coroutine CPlayerControler;

        public void Init(Entity _entity)
        {
            Entity entity = _entity;
            unit = entity as Unit;
            StartCoroutine(CPlayerWASDControler());
            // добавить управление Text на 20 секунд
        }
        public void PostInit()
        {
            StartCoroutine(CPlayerWASDControler());
        }
        public void StatePlayerController(bool on)
        {
            if (on) { CPlayerControler = StartCoroutine(CPlayerWASDControler()); }
            else
            {
                if (CPlayerControler != null) StopCoroutine(CPlayerControler);
            }
        }

        IEnumerator CPlayerWASDControler()
        {
            while (true)
            {
                yield return null;

                Vector3 direction = new Vector3(0, 0, 0);
                if (Input.GetKeyDown(KeyCode.A))
                {
                    Vertical = -0.9f;
                }
                if (Input.GetKeyDown(KeyCode.D))
                {
                    Vertical = 0.9f;
                }

                if (Input.GetKeyUp(KeyCode.A) && Vertical < 0)
                {
                    Vertical = 0;
                }
                if (Input.GetKeyUp(KeyCode.D) && Vertical > 0)
                {
                    Vertical = 0;
                }

                //////////
                if (Input.GetKeyDown(KeyCode.W))
                {
                    Horizontal = 0.9f;
                }
                if (Input.GetKeyDown(KeyCode.S))
                {
                    Horizontal = -0.9f;
                }

                if (Input.GetKeyUp(KeyCode.W) && Horizontal > 0)
                {
                    Horizontal = 0;
                }
                if (Input.GetKeyUp(KeyCode.S) && Horizontal < 0)
                {
                    Horizontal = 0;
                }
                direction = Vector3.forward * Horizontal + Vector3.right * Vertical;

                unit.rb.AddForce(direction * unit.movement.speed * 2 * Time.fixedDeltaTime, ForceMode.VelocityChange);

                if (Input.GetKeyDown(KeyCode.C))
                {
                    unit.attack.Fire();
                }

            }
        }

    } 