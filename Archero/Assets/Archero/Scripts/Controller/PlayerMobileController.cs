using System.Collections;
using System.Collections.Generic;
using Orcan.Unit;
using UnityEngine;
using Zenject;

public class PlayerMobileController : MonoBehaviour, IControllerUnit
{
    Unit unit;
    VariableJoystick variableJoystick;
    Coroutine CPlayerControler;

     
    public void Init(Entity _entity, VariableJoystick _variableJoystick)
    {
        Entity entity = _entity;
        unit = entity as Unit;
        StartCoroutine(CPlayerJoystickControler());
        variableJoystick = _variableJoystick; 
        // добавить управление Text на 20 секунд 
    }
    public void PostInit()
    {
        StartCoroutine(CPlayerJoystickControler());
    }
    public void StatePlayerController(bool on)
    {
        if (on) { CPlayerControler = StartCoroutine(CPlayerJoystickControler()); }
        else
        {
            if (CPlayerControler != null) StopCoroutine(CPlayerControler);
        }
    }

    IEnumerator CPlayerJoystickControler()
    {
        while (true)
        {
            yield return null;
            Vector3 direction = Vector3.forward * variableJoystick.Vertical + Vector3.right * variableJoystick.Horizontal;
            unit.rb.AddForce(direction * unit.movement.speed * 2 * Time.fixedDeltaTime, ForceMode.VelocityChange);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                unit.attack.Fire();
            }
        }
    } 
}
